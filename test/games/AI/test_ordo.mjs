import OpenXum from '../../../lib/openxum-core';
import AI from '../../../lib/openxum-ai';

let e = new OpenXum.Ordo.Engine(OpenXum.Ordo.GameType.STANDARD, OpenXum.Ordo.Color.WHITE);
let p1 = new AI.Generic.RandomPlayer(OpenXum.Ordo.Color.WHITE, OpenXum.Ordo.Color.BLACK, e);
let p2 = new AI.Generic.RandomPlayer(OpenXum.Ordo.Color.BLACK, OpenXum.Ordo.Color.WHITE, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
  let move = p.move();

  if (move.constructor === Array) {
    for (let i = 0; i < move.length; ++i) {
      console.log(move[i].to_string());
    }
  } else {
    console.log(move.to_string());
  }

  moves.push(move);
  e.move(move);
  p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === OpenXum.Ordo.Color.BLACK ? "black" : "white"));