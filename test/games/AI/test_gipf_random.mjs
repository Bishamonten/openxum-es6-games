import OpenXum from '../../../lib/openxum-core';
import AI from '../../../lib/openxum-ai';

let e = new OpenXum.Gipf.Engine(OpenXum.Gipf.GameType.STANDARD, OpenXum.Gipf.Color.BLACK);
let p1 = new AI.Generic.RandomPlayer(OpenXum.Gipf.Color.BLACK, OpenXum.Gipf.Color.WHITE, e);
let p2 = new AI.Generic.RandomPlayer(OpenXum.Gipf.Color.WHITE, OpenXum.Gipf.Color.BLACK, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
  let move = p.move();

  if (move.constructor === Array) {
    for (let i = 0; i < move.length; ++i) {
      console.log(move[i].to_string());
    }
  } else {
    console.log(move.to_string());
  }

  moves.push(move);
  e.move(move);
  p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === OpenXum.Gipf.Color.BLACK ? "black" : "white"));